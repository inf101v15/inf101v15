import java.util.*;

class Oppgave1
{
	public static void main(String[] args)
	{
                Scanner tastatur = new Scanner(System.in);

                int feil = 0;
                char ferdig = 'n';
		int diameter;

		Roer foerste = null;
		Roer siste = null;
		Roer temp = null;

                while(ferdig == 'n') { // Mens vi ønsker å legge til flere rør:
                        System.out.print("Diameter til nytt rør: ");

                        diameter = tastatur.nextInt();

			if(siste != null) { // Hvis ikke første:
				siste.koblePaa(new Roer(diameter)); // Koble på et nytt rør.

				siste = siste.nesteRoer(); // Oppdatere hvilket rør som er siste.
			}
			else { // Hvis første rør:
				siste = new Roer(diameter); // Opprett første rør.
				foerste = siste; // Første og siste er samme.
			}

                        System.out.print("Er du ferdig (j/n)? ");

                        ferdig = tastatur.next().charAt(0);
                }

		System.out.print("Ballens diameter: ");
                diameter = tastatur.nextInt();

		int lengde = 1;

		temp = foerste;
		while(temp != null) { // Mens det er flere rør igjen.
			if(temp.hentDiameter() < diameter) { // Sjekk røret sin diameter i forhold til ballen.
				System.out.println("Rør " + lengde + " med diameter " + temp.hentDiameter() + " er for lite!");

                		tastatur.close();

				System.exit(0);
			}

			temp = temp.nesteRoer();

			lengde++;
		}

		System.out.println("Ballen passerte alle " + (lengde - 1) + " rørene uten problemer.");

                tastatur.close();
	}
}

/**
 * Klassen Roer representerer et segment av det totale røret.
 * Den inneholder en referanse til neste rør.
 **/

class Roer
{
	private int diameter; 
	private Roer neste; // Referanse til neste rør.

	Roer(int diameter)
	{
		this.diameter = diameter;

		System.out.println("Opprettet et rør med diameter " + diameter + ".");

		neste = null;
	}

	Roer nesteRoer()
	{
		return neste;
	}

	void koblePaa(Roer neste)
	{
		this.neste = neste;
	}

	int hentDiameter()
	{
		return diameter;
	}
}
