package inf100.Roereksempel;

public class Ball {
	
	private int diameter;

	public Ball(int diameter) {
		this.diameter = diameter;
	}

	public int hentDiameter() {
		return diameter;
	}

}
