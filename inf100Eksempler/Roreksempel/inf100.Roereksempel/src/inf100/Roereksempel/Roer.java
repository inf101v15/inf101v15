package inf100.Roereksempel;

/**
 * Klassen Roer representerer et segment av det totale røret. Den inneholder en
 * referanse til neste rør.
 **/

class Roer {
	private int diameter;
	private Roer neste; // Referanse til neste rør.

	Roer(int diameter) {
		this.diameter = diameter;

		System.out.println("Opprettet et rør med diameter " + diameter + ".");

		neste = null;
	}

	Roer nesteRoer() {
		return neste;
	}

	void koblePaa(Roer neste) {
		this.neste = neste;
	}

	int hentDiameter() {
		return diameter;
	}
}
