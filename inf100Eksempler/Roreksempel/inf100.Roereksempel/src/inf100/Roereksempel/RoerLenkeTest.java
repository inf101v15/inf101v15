package inf100.Roereksempel;

import static org.junit.Assert.*;
import org.junit.Test;

public class RoerLenkeTest {

	@Test
	public void testInitLengde() {
		RoerLenke lenke = new RoerLenke();
		assertEquals(0,lenke.getLengde());
	}
	
	@Test
	public void testTrillBall() {
		RoerLenke lenke = new RoerLenke();
		lenke.leggTilRoer(new Roer(1));
		lenke.leggTilRoer(new Roer(2));
		assertEquals(2,lenke.trillBall(new Ball(1)));
	}
	
	@Test
	public void testTrillBall2() {
		RoerLenke lenke = new RoerLenke();
		assertEquals(0,lenke.trillBall(new Ball(1)));
	}


}
