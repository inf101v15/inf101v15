package inf100.Roereksempel;

import java.util.Scanner;

class RoerLenke {
	private Roer foersteRoer;
	private Roer sisteRoer;
	private Roer roerMedBall;
	private int lengde = 0;

	public int getLengde() {
		return lengde;
	}

	/**
	 * Legger til et Roer bakerst i RoerLenken.
	 * @param r
	 */
	void leggTilRoer(Roer r) {
		if (foersteRoer == null) {
			foersteRoer = r;
			sisteRoer = r;
			lengde++;
		} else {
			sisteRoer.koblePaa(r);
		}
	}

	/**
	 * Triller ballen gjennom rørerene slik at
	 * den stopper ved det første røret som er har
	 * mindre diameter.
	 * @param ball
	 * @return antall Roer som ballen trillet gjennom.
	 */
	 int trillBall(Ball ball) {
		roerMedBall = foersteRoer;
		int antallTrilletGjennom = 0;
		while (roerMedBall != null) {
			if (roerMedBall.hentDiameter() < ball.hentDiameter()) {
				return antallTrilletGjennom;
			}

			antallTrilletGjennom++;
			roerMedBall = roerMedBall.nesteRoer();
		}
		return antallTrilletGjennom;
	}
	 
	private Roer getRoerMedBall() {
		return roerMedBall;
	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		char ferdig = 'n';
		
		RoerLenke lenke = new RoerLenke();

		while (ferdig == 'n') { // Mens vi ønsker å legge til flere rør:
			System.out.print("Diameter til nytt rør: ");

			lenke.leggTilRoer(new Roer(tastatur.nextInt()));

			System.out.print("Er du ferdig (j/n)? ");

			ferdig = tastatur.next().charAt(0);
		}

		System.out.print("Ballens diameter: ");
		int antallTrillet = lenke.trillBall(new Ball(tastatur.nextInt()));
		tastatur.close();

		if (antallTrillet == lenke.getLengde()) {
			System.out.println("Ballen passerte alle " + lenke.getLengde()
					+ " rørene uten problemer.");
		} else {
			System.out.println("Rør " + (antallTrillet + 1) + " med diameter "
					+ lenke.getRoerMedBall().hentDiameter() + " er for lite!");
		}
	}
}
