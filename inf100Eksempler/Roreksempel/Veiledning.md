RørLenke
=====


Vi skal her se nøyere på koden fra oppgave 1, øving 4 i INF100 H2014,
og vi kommer til å bruke Eclipse.

Vi har som mål å

  * Vise frem nyttige funksjoner fra eclipse.
  * Introdusere testing.

Vi tar utganspunkt i koden som er i [Losningsforslag-mappen](Losningsforslag/), og bruker så 
eclipse til å omforme koden slik at det er lettere å teste programmet. Tilslutt
introduserer vi testingverktøyet JUnit og skriver noen enkle tester for programmet.


Bruk Av Eclipse
------

Først start eclipse og lag et nytt prosjekt (File -> new -> Java Project),
kall prosjektet "inf100.Roereksempel" og lag en ny klasse i prosjektet
(høyre klikk src og velg new->Class) og kall den Oppgave1. Pass på at det står
inf100.Roereksempel under package. Nå har vi en klasse i prosjektet vårt hvor vi
kan kopiere inn [løsningsforslaget fra oppgaven](Losningsforslag/Oppgave1.java)
Merk at øverst står det
`package inf100.Roereksempel`. Pakker var ikke en del av inf100 så vi skal ikke
gå nærmere inn på dette nå, men resten skal vi bytte ut med inneholde fra Losningsforslaget.

La oss kjøre koden for å sjekke at det fortsatt fungerer! Øverst finner du en play knapp
og når du trykker på den vil koden kjøre og du kan interagere med terminalen i det
nederste vinduet.


Refaktorering
------------

Først kan det hende at indenteringen ikke er riktig. Det er fordi den opprinnelige filen
blander mellomrom og tabulator. La oss fikse dette (og andre små formateringsfeil) ved
å trykke shift-ctrl-f (Source → Format). Dette gjør også om linjedeling av strenger noen steder og forandrer
plassering av krøllparanteser. I inf101 vil vi at dere følger formateringstilen til 
Eclipse.

Neste stil-problem med denne koden er at filen inneholder to klasser, Roer og Oppgave1.
I inf101 vil vi at dere har en klasse per fil. Marker Roer klassen, høyre klikk, Refactor->
move to own file.

Neste ting er at filen importerer Java.Util.* men bruker bare Java.Util.Scanner. Det
er som ofte greiest å la Eclipse organisere importering: Source -> Organize imports.

Denne koden blander håndering av terminalen og logikken til programmet. Det er flere grunner
til at dette er en dårlig ide, men hovedsaklig gjør det at koden er lite gjenbrukbar.
Hva hvis vi har lyst til å kjøre programmet uten å interagere med terminalen, feks. via
en webside? En slik gjenbruk er testing og siden vi skal snakke om testing senere er det
best at vi prøver å separere terminalbruken fra logikken nå. 

La oss skifte navnet på klassen med en gang slik at vi viser hensikten med programmet.
Høyre-klikk på Oppgave1, velg Refactor->Rename og kall klassen ```RoerLenke```. Tanken er nå å
flytte de lokale variablene ```lengde```, ```foerste```, ```siste```, og ```temp``` til feltvariabler og lage metoder
```leggTilRoer()```, ```trillBall()``` som erstatter noe av logikken som er gjemt i main-metoden. For hver
av variablene ```lengde```, ```foerste```, ```siste```, og ```temp```, høyre-klikk og velg refactor->convert to field
variable. Du kan da også gi de litt bedre navn om du vil (Refactor->Rename, eller shift-alt-r), la oss kalle temp for ```roerMedBallI```
siden ```temp``` er et forferdelig navn. 

Nå har vi statiske felt variabler for hver de viktigste tingene. Vi kommer til å bytte
de ut med instanse-variabler til slutt, men først vil vi ekstrahere logikken fra programmet.
Marker koden fra if-else setingen som begynner med ```if(siste != null)```, og gå til refactor->
extract method. Dette blir en metode kalt leggTilRoer og har en parameter, diameter. Det samme
kan du gjøre med while løkken (begynner med ```RoerMedBallI = foerste;```) lengre ned som vi vil kalle
```trillBall()```. Merk at denne får en ekstra parameter ```Scanner tastatur``` siden man i denne løkken
vil lukke Scanneren og så avslutte kjøringen av programmet hvis ballen stopper. Dette ødelegger
veldig for gjenbrukbarheten av ```trillBall```! Det er ikke vanskelig å se for seg et tilfelle vi 
har lyst til å kalle ```trillBall``` uten å stoppe kjøringen av hele programmet når ballen stopper. Så
la oss bytte ut ```exit``` med ```break```. Da kan vi også fjerne bruken av tastatur fra denne metoden.

Kjør programmet igjen for å sjekke at det fungerer. Hva skjer hvis et rør
har mindre diameter enn ballen? Da fungerer det selvfølgelig ikke fordi ```break;```
er ikke det samme som exit. La oss skifte litt på ```trillBall``` slik at den returnerer
hvor mange rør som det ble trillet igjennom. Da kan vi også skifte ```lengde``` til å bety
hvor mange rør det er i lenken og ikke hvor mange rør ballen gikk gjennom. Da må
```leggTilRoer``` også oppdateres. Slutten av main-metoden blir da

		int antallTrillet = trillBall(diameter);

		if(antallTrillet == lengde){
		System.out.println("Ballen passerte alle " + lengde
				+ " rørene uten problemer.");
		} else {
			System.out.println("Rør " + (antallTrillet + 1) + " med diameter "
					+ RoerMedBallI.hentDiameter() + " er for lite!");
		}


Helt til slutt ønsker vi at de statiske variablene og metodene vi har laget skal 
være instanse-variabler. Fjern nøkkelordet ```static```, opprett et objekt av klassen
```RoerLenke``` i main metoden og gjør om koden slik at metodene er kalt på det objektet.
Gjør metodene også ```public```. Vi kommer til å trenge getters for ```lengde``` og ```roerMedBallI```.
Det kan genereres med source->Generate getters and setters (fjern avkryssingen på setterne, slik at du bare får med getterne).

Nå har vi trukket ut logikken av programmet fra main metoden og har istedenfor et
objekt ```RoerLenke``` med metodene ```leggTilRoer()```, ```trillBall()```, ```getLengde()``` og ```getRoerMedBallI()```.

Testing
=================

Når vi gjør om på koden som nå, har vi ofte lyst til å kjøre det på nytt for
å sjekke at den fremdeles fungerer. Vi støter da på flere problemer. For det første
tar det lang tid å utføre kjøringen. Siden det tar lang tid har vi ikke mulighet til
å variere input mye. Dessuten kan det hende at koden vi har lyst til sjekke at fungerer
ikke er satt inn i kjøringen av programmet enda. Vi ender opp med å forandre programmet
for å sjekke at det fungerer, som tar mer tid og kan introdusere flere feil.

En løsning på dette er det som kalles testing. Vi kommer til å bruke et testingsverktøy
som kalles JUnit. Høyre klikk på prosjektet i Package Explorer, velg BuildPath -> Add Library og legg
til biblioteket JUnit (hvis du glemmer dette, vil Eclipse automatisk forslå det for deg).
Så høyreklikk på klassen RoerLenke og velg new->JUnit test case. Kall
den nye testklassen RoerLenkeTest.

RoerLenkeTest inneholder en metode ```test()```, med annoteringen ```@Test```. Hvis du nå trykker på play-knappen
vil alle metoder i ```RoerLenkeTest``` med denne annoteringen kjøres. Da kommer det opp en JUnit fane
til venstre for koden og der står det at det har vært 1 failure. Dette er fordi ```test()``` kaller metoden
```fail("Not yet implemented")```. Vi kan istedenfor skrive en egen test:

    @Test
    public void testInitLengde() {
    	RoerLenke lenke = new RoerLenke();
    	if(0 != lenke.getLengde()){
        fail("Ny RoerLenke har ikke 0 som lengde");
      }
    }

Hvis vi nå kjører RoerLenkeTest får vi ingen feilmeldinger. På denne måten kan vi bytte ut vår manuelle
kjøring av programmet med en automatisk sjekk. For å gjøre sjekking av parametere litt mer lettvindt
kommer JUnit med assert-setninger, slik at vi ikke alltid må skrive en if-setning etterfulgt av en ```fail```:

    @Test
    public void testTrillBall() {
    	RoerLenke lenke = new RoerLenke();
    	lenke.leggTilRoer(1);
    	lenke.leggTilRoer(2);
    	assertEquals(2,lenke.trillBall(1));
    }

Testen over tilsvarer mer eller mindre en vanlig kjøring av programmet slik vi ville gjort for hånd, og den
tar mindre enn et sekund å kjøre. Å taste inn alle tallene og så visuelt inspisere resultatet tar 
kanskje bare noen sekunder, men på denne måten kan vi gjenta denne testen ofte. Desuten med mer komplekse
problemer kan vi kjøre en mengde tester som det ville tatt dagevis å gjøre for hånd.


