Eksempler fra Inf100
===================


Denne mappen inneholder oppgaver fra inf100 der
programmeringsstilen er forandret for å legge vekt
på

  * Enkapsulering og Abstraksjon vha. objekter.
  * Automatisk testing.
  * Vise bruk av eclipse.
