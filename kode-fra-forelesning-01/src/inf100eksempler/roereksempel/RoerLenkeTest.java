package inf100eksempler.roereksempel;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

public class RoerLenkeTest {
	private Random random = new Random();
	
	@Test
	public void testLengdeTilfeldig() {
		for(int i = 0; i < 10; i++) {
			RoerLenke roer = new RoerLenke();
			int n = random.nextInt(1000);
			for(int k = 0; k < n; k++)
				roer.leggTilRoer(10);
			
			lengdeEgenskap(roer);
		}
	}

	@Test
	public void testLengde() {
		RoerLenke roer = new RoerLenke();
		int n = 1000;
		for(int i = 0; i < n; i++) {
			lengdeEgenskap(roer);
		}
		
		assertEquals(n, roer.getLengde());
	}


	@Test
	public void test2() {
		RoerLenke roer = new RoerLenke();
		roer.leggTilRoer(10);
		lengdeEgenskap(roer);
	}

	private void lengdeEgenskap(RoerLenke roer) {
		int l = roer.getLengde();
		roer.leggTilRoer(10);
		assertEquals(l+1, roer.getLengde());
	}
	
	
	@Test
	public void testGetFoerste1() {
		RoerLenke roerLenke = new RoerLenke();
		Roer femRoer = new Roer(5);
		Roer femtenRoer = new Roer(15);
		Roer syttenRoer = new Roer(17);
		roerLenke.leggTilRoer(femRoer);
		roerLenke.leggTilRoer(femtenRoer);
		roerLenke.leggTilRoer(syttenRoer);
		
		assertEquals(femRoer, roerLenke.getFoerste());
	}

	@Test
	public void testGetRoer() {
		RoerLenke roerLenke = new RoerLenke();
		Roer femRoer = new Roer(5);
		Roer femtenRoer = new Roer(15);
		Roer syttenRoer = new Roer(17);
		roerLenke.leggTilRoer(femRoer);
		roerLenke.leggTilRoer(femtenRoer);
		roerLenke.leggTilRoer(syttenRoer);
		
		assertEquals(femRoer, roerLenke.getRoer(0));
		assertEquals(femtenRoer, roerLenke.getRoer(1));
		assertEquals(syttenRoer, roerLenke.getRoer(2));
	}

	@Test
	public void testGetFoerste2() {
		RoerLenke roerLenke = new RoerLenke();
		roerLenke.leggTilRoer(5);
		roerLenke.leggTilRoer(15);
		roerLenke.leggTilRoer(17);
		
		assertEquals(5, roerLenke.getFoerste().hentDiameter());
	}


}
