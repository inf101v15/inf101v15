package inf100eksempler.roereksempel;

public class RoerUtforsker {
	private RoerLenke roerLenke;
	
	public RoerUtforsker(RoerLenke roerLenke) {
		super();
		this.roerLenke = roerLenke;
	}
	
	public static void main(String[] args) {
		RoerLenke roerLenke = new RoerLenke();
		roerLenke.leggTilRoer(15);
		roerLenke.leggTilRoer(5);
		roerLenke.leggTilRoer(17);

		RoerUtforsker roerUtforsker = new RoerUtforsker(roerLenke);
		
		roerUtforsker.utforsk();
		
	}

	public void utforskRoer(Roer roeret, int n) {
		System.out.println("Vi er i rør nr. " + n  + " som har diameter " + roeret.hentDiameter());
	}
	
	public void utforsk() {
		int antall = 0;
		Roer detteRoeret = roerLenke.getFoerste();
		
		while (detteRoeret != null) { // Mens det er flere rør igjen.
			utforskRoer(detteRoeret, antall);
			
			
			detteRoeret = detteRoeret.nesteRoer();
			antall++;
		}
	}
}
