package inf100eksempler.roereksempel;

/**
 * Klassen Roer representerer et segment av det totale røret. Den inneholder en
 * referanse til neste rør.
 **/

public class Roer {
	private int diameter;
	private Roer neste; // Referanse til neste rør.

	public Roer(int diameter) {
		this.diameter = diameter;

		//System.out.println("Opprettet et rør med diameter " + diameter + ".");

		neste = null;
	}

	public Roer nesteRoer() {
		return neste;
	}

	public void koblePaa(Roer neste) {
		this.neste = neste;
	}

	public int hentDiameter() {
		return diameter;
	}
}