package inf101.list;

import java.util.Arrays;

/**
 * En implementasjon av IList som bruker tabeller.
 */
public class ArrayList implements IList {
	private int[] data;
	private int length;
	
	/**
	 * Create a new ArrayList with the given capacity.
	 * @param size Max capacity of the list
	 */
	public ArrayList(int size) {
		data = new int[size];
		length = 0;
	}
	
	@Override
	public int getLength() {
		return length;
	}

	@Override
	public int get(int index) {
		if(index >= length)
			throw new IndexOutOfBoundsException("" + index);
		return data[index];
	}

	@Override
	public int set(int index, int num) {
		if(index >= length)
			throw new IndexOutOfBoundsException("" + index);
		int r = data[index];
		data[index] = num;
		return r;
	}

	@Override
	public boolean isEmpty() {
		return length == 0;
	}

	@Override
	public void add(int num) {
		if(length >= data.length)
			throw new IndexOutOfBoundsException("" + length);
			
		data[length] = num;
		
		length = length + 1;
		
		Arrays.copyOf(data, length + 100);
	}

}
