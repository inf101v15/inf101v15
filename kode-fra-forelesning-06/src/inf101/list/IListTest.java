package inf101.list;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IListTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void emptyTest() {
		ArrayList list = new ArrayList(10);
		
		emptyProperty(list);
		
		list.add(2);
		list.add(3);
		
		emptyProperty(list);
	}

	@Test
	public void outOfBoundsTest() {
		ArrayList list = new ArrayList(10);
		
		try {
			list.set(1, 4);
			fail("Should throw exception");
		}
		catch(ArrayIndexOutOfBoundsException e) {
			;
		}
	}
	
	@Test 
	public void newEmptyTest() {
		ArrayList list = new ArrayList(10);
		
		assertTrue(list.isEmpty());
	}
	
	
	@Test
	public void setGetTest() {
		ArrayList list = new ArrayList(10);
			
		list.add(2);
		list.add(3);

		setGetProperty(list, 0, 53);
		setGetProperty(list, 1, 47);
	}
	

	@Test
	public void addGetTest() {
		ArrayList list = new ArrayList(10);
	
		addGetProperty(list, 53);
		addGetProperty(list, 47);
	}

	@Test
	public void addLengthTest() {
		ArrayList list = new ArrayList(10);
	
		addLengthProperty(list, 53);
		addLengthProperty(list, 47);
	}

	private void emptyProperty(IList list) {
		assertEquals(list.isEmpty(), list.getLength() == 0);
	}
	
	private void setGetProperty(IList list, int index, int num) {
		list.set(index, num);
		assertEquals(num, list.get(index));
	}
	
	private void addGetProperty(IList list, int num) {
		int pos = list.getLength();
		
		list.add(num);
		
		assertEquals(num, list.get(pos));
	}
	
	private void addLengthProperty(IList list, int num) {
		int n = list.getLength();
		list.add(num);
		assertEquals(n+1, list.getLength());
	}
}
