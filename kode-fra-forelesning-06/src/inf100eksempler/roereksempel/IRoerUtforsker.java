package inf100eksempler.roereksempel;

/**
 * Interface for utforskning at ett enkelt rør.
 *
 */
public interface IRoerUtforsker {

	void utforskRoer(Roer roeret, int n);

}