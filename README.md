# Velkommen til INF101 ved UiB Våren 2015

Kursinformasjon finner du på [Wikien](https://bitbucket.org/inf101v15/inf101v15/wiki/Home).

(Se også [Mi Side](https://miside.uib.no/dotlrn/classes/det-matematisk-naturvitenskapelige-fakultet/inf101/inf101-2015v/) (krever innlogging).)
