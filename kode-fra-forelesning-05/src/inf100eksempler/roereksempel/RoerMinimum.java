package inf100eksempler.roereksempel;

/**
 *  En rørutforsker som finner diameteren på det minste røret i en lenke.
 */
public class RoerMinimum implements IRoerUtforsker { 
	private int minimum;

	public RoerMinimum() {
		minimum = Integer.MAX_VALUE;
	}
	
	@Override
	public void utforskRoer(Roer roeret, int n) {
		if(roeret.hentDiameter() < minimum)
			minimum = roeret.hentDiameter();
	}

	public int getMinimum() {
		return minimum;
	}
}
