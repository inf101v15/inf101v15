package inf100eksempler.roereksempel;

/**
 * Rørutforsker som skriver ut informasjon om hvert rør.
 */
public class RoerInformasjonsUtskriver implements IRoerUtforsker {
	/* (non-Javadoc)
	 * @see inf100eksempler.roereksempel.IRoerUtforsker#utforskRoer(inf100eksempler.roereksempel.Roer, int)
	 */
	@Override
	public void utforskRoer(Roer roeret, int n) {
		System.out.println("Vi er i rør nr. " + n  + " som har diameter " + roeret.hentDiameter());
	}
}
