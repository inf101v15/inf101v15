package inf100eksempler.roereksempel;

import java.util.Scanner;

/**
 * En kjede av rør.
 */
class RoerLenke {
	private Roer foerste = null;
	private Roer siste = null;
	private Roer roerMedBallI = null;
	private int lengde = 0;

	public Roer getFoerste() {
		return foerste;
	}

	public Roer getRoer(int i) {
		if (i >= lengde)
			throw new IndexOutOfBoundsException("" + i);

		Roer resultat = foerste;
		for (int j = 0; j < i; j++)
			resultat = resultat.nesteRoer();

		return resultat;
	}

	/**
	 * @return Det røret ballen stoppet opp i, eller null hvis ballen gikk
	 *         gjennom hele lenken.
	 */
	public Roer getRoerMedBallI() {
		return roerMedBallI;
	}

	public int getLengde() {
		return lengde;
	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		RoerLenke roerLenke = new RoerLenke();
		char ferdig = 'n';
		int diameter;

		while (ferdig == 'n') { // Mens vi ønsker å legge til flere rør:
			System.out.print("Diameter til nytt rør: ");

			diameter = tastatur.nextInt();

			roerLenke.leggTilRoer(diameter);

			System.out.print("Er du ferdig (j/n)? ");

			ferdig = tastatur.next().charAt(0);
		}

		System.out.println("Rørets lengde er: " + roerLenke.lengde);

		System.out.print("Ballens diameter: ");
		diameter = tastatur.nextInt();

		int antallTrillet = roerLenke.rullBall(diameter);

		if (antallTrillet == roerLenke.lengde) {
			System.out.println("Ballen passerte alle " + roerLenke.lengde
					+ " rørene uten problemer.");
		} else {
			System.out.println("Rør " + antallTrillet + " med diameter "
					+ roerLenke.roerMedBallI.hentDiameter() + " er for lite!");
		}
		tastatur.close();
	}

	/**
	 * Prøver å rulle en ball gjennom lenken.
	 * 
	 * Hvis ballen stoppet, vil {@link #getRoerMedBallI()} fortelle hvilket rør
	 * den stoppet i.
	 * 
	 * @param diameter
	 *            Ballens diameter
	 * @return Antall rør ballen rullet gjennom før den evt stoppet
	 */
	public int rullBall(int diameter) {
		int antallTrillet = 0;
		roerMedBallI = foerste;
		while (roerMedBallI != null) { // Mens det er flere rør igjen.
			if (roerMedBallI.hentDiameter() < diameter) { // Sjekk røret sin
															// diameter i
				// forhold til ballen.

				break;
			}

			roerMedBallI = roerMedBallI.nesteRoer();

			antallTrillet++;
		}
		return antallTrillet;
	}

	public void leggTilRoer(int diameter) {
		leggTilRoer(new Roer(diameter));
	}

	public void leggTilRoer(Roer roer) {
		if (siste != null) { // Hvis ikke første:
			siste.koblePaa(roer); // Koble på et nytt rør.

			siste = siste.nesteRoer(); // Oppdatere hvilket rør som er
										// siste.
		} else { // Hvis første rør:
			siste = roer; // Opprett første rør.
			foerste = siste; // Første og siste er samme.
		}

		lengde++;
	}

}
