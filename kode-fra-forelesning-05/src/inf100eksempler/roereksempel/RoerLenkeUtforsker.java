package inf100eksempler.roereksempel;

/**
 * Klasse som går gjennom et rør, og kjører en IRoerUtforsker på hvert rør i lenken.
 * 
 */
public class RoerLenkeUtforsker {
	private RoerLenke roerLenke;
	private IRoerUtforsker roerUtforsker;

	public RoerLenkeUtforsker(RoerLenke roerLenke, IRoerUtforsker utforsker) {
		super();
		this.roerLenke = roerLenke;
		this.roerUtforsker = utforsker;
	}

	// eksempel
	public static void main(String[] args) {
		RoerLenke roerLenke = new RoerLenke();
		roerLenke.leggTilRoer(15);
		roerLenke.leggTilRoer(5);
		roerLenke.leggTilRoer(17);

		RoerMinimum roerMinimum = new RoerMinimum();

		RoerLenkeUtforsker roerUtforsker = new RoerLenkeUtforsker(roerLenke,
				new RoerInformasjonsUtskriver());

		roerUtforsker.utforskLenke();

		roerUtforsker = new RoerLenkeUtforsker(roerLenke, roerMinimum);

		roerUtforsker.utforskLenke();

		System.out.println("Det minste røret var " + roerMinimum.getMinimum()
				+ " stort");
	}

	public void utforskLenke() {
		int antall = 0;
		Roer detteRoeret = roerLenke.getFoerste();

		while (detteRoeret != null) { // Mens det er flere rør igjen.
			// hvis vi vil, kan vi utforske bare annet hvert rør:
			// if (antall % 2 == 0)
			roerUtforsker.utforskRoer(detteRoeret, antall);

			detteRoeret = detteRoeret.nesteRoer();
			antall++;
		}
	}
}
