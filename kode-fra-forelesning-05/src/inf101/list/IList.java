package inf101.list;

/**
 * En liste er en ordnet sekvens av elementer, indeksert fra 0 til getLength()-1.
 * 
 */
public interface IList {

	/**
	 * Get length of list.
	 * 
	 * @return Length of the list
	 */
	int getLength();
	
	/**
	 * Get the value at a position.
	 * 
	 * @param index The position to examine
	 * @return The value at that position
	 * @throws IndexOutOfBoundsException if index is not >= 0 and < getLength()
	 */
	int get(int index);
	
	/**
	 * Update the value at a position.
	 * 
	 * @param index The position to update
	 * @param num The value to write
	 * @return The previous value stored at that location.
	 * @throws IndexOutOfBoundsException if index is not >= 0 and < getLength()
	 * @property after set(i,x), get(i) will return x.
	 * 
	 */
	int set(int index, int num);
	
	/**
	 * Insert an element at position {@link #getLength()}
	 * 
	 * 
	 * @param num The value to add
	 * @throws ArrayIndexOutOfBoundsException if the list's capacity is exceeded
	 * @property After add, length of list will have increased by one.
	 * @property After add(n), get(getLength()-1) == n 
	 */
	void add(int num);
	
	/**
	 * Check if list is empty.
	 * 
	 * @return True if the list is empty.
	 * @property isEmpty() iff getLength() == 0
	 */
	boolean isEmpty();
}
